# Markdown format

## Pre-requisites

```
Node version = 14.2.0

```


## How to run the server?

```
// This installs all npms from package.json
> npm install

> node server.js

```

## Access the application at 

```
http://localhost:3000/

```

## Installing new node modules

```

If you are installing new modules:

> npm install --save moduleName

> npm install --save-dev moduleNameUsedForTestingOnly

```
