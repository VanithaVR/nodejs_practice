// Using 'express js' middleware
const express       = require('express');
const bodyParser    = require('body-parser');

// Create a server from 'express js'
const server    = express();

// Add body parser to server (adding body parser middleware)
server.use(bodyParser.urlencoded({ extended: false }));

// Asking to parse the input with application/json format
server.use(bodyParser.json());

// Add routes to the server
server.get( '/employee', function(request, response){
    response.send({"message":"Welcome", "employee": {"firstName":"Raju", "lastName":"R", "id":"678978" }})
} )

server.post( '/employee', function(request, response){
    let empTask = request.body;
    
    response.send({
        "message": "Welcome "+empTask.firstName
    });
} )

server.listen(3000, function(){
    console.log("Server started at port 3000")
})

